App Functions
---

- Continuous Imagery Taking Mode
	+ Take continuous imagery just like the current Mapillary app 
	+ Add a `Mark` as you travel down the road - One Button
	+ Photos are uploaded directly to Mapillary and deleted from the device

- Still Image Taking Mode
	+ Take continuous imagery just like the current Mapillary app 
	+ Add a `Mark` as you travel down the road - One Button
	+ Photos are uploaded directly to Mapillary and deleted from the device

- Mark Review Mode
	+ Before upload we can review the `Mark` and turn it into a flag or a feature
	+ Move the bookmarks between photos

- Feature/Flag Creation/Edit  Mode
	+ Create a feature / Flag from scratch or from a Mark
	+ Uploads to map.safe7y database
	+ All the same drop downs as map.safe7y
	+ Can review Mapillary images as well as local (non-uploaded) images
	+ Must be usable offline
	+ Associate a zoomed in view of the flag with a 

- View Mode

- User Sign In
	+ Mapillary account and a safe7y.com account