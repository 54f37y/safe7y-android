Overview
===

Safety in Numbers is committed to helping road operators find damaged, mis-installed and missign road safety hardware and helping them fix it to save lives. 

This app allows the general public to capture street level imagery and flag issues they see as they travel the roads.

This project is still in it's early stages and the repo currently contains ideas and resources to create this app.


Ideas
===

- Take regular photos (speed and time based capture options as well as singles)
- Save location data in the EXIF data (like Maipllary)
- Button to flag issues
- Voice command to flag issues
- Upload images to AWS S3 (wifi by default, ask for cell)
- Upload to Mapillary serverside
- Eventually include AI in the app to identify objects of interest

Resources
===

- https://github.com/mapillary/mapillary-sdk-ios
- https://android-releases.mapillary.com/sdk/Mapillary_SDK_Quick%20start%20guide.pdf
- https://github.com/kartaview/android
- https://github.com/CameraKit/camerakit-android
- https://github.com/developerxrahul/amazon-s3-android-uploader
